/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import View.MainView;
import helpers.FileWalker;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.JFileChooser;
import model.EditorModel;

/**
 *
 * @author RLBrown
 */
public class MainController {
    
    private MainView view;
    private EditorModel model;
    private FileWalker fw;
    
    public MainController() {
        this.view = new MainView();
        this.view.setVisible(true);
        this.model = new EditorModel();
        this.fw = new FileWalker();
        this.view.setController(this);
    }
    
    public String setRootDirectory() {
        
        JFileChooser chooser = this.model.getChooser();
        int result;
        String rootDirPath = null;
        File selectedFile = null;
        
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setCurrentDirectory(new File(System.getProperty("user.home")));
        
        result = chooser.showOpenDialog(view);
        
        if ( result == JFileChooser.APPROVE_OPTION ) {
            selectedFile = chooser.getSelectedFile();
            rootDirPath = selectedFile.getAbsolutePath();
            this.model.setRootPathForSearch(rootDirPath);
            this.view.getjTextField1().setText(rootDirPath);
        }
        
        return rootDirPath;
        
    } // end method
    
    public void setFilterDirectories(String s) {
        
        if ( s == null ) {
            this.model.setFilterDirs(null);
            return;
        }
        
        ArrayList<String> filterList = new ArrayList<>(Arrays.asList(s.split(",")));
        
        for ( int i = 0; i < filterList.size(); i++ ) {
            String z = filterList.get(i);
            filterList.set(i, z.trim());
        }
        
        this.model.setFilterDirs(filterList);
        
    } // end method
    
    public void setSearchContext(String s) {
        
        this.model.setInitialContext(s);
        ArrayList<String> cList = new ArrayList<>(Arrays.asList(s.split("\n")));
        this.model.setInitialContextList(cList);
        
    } // end method
    
    public void setReplacement(String s) {
        
        this.model.setReplacement(s);
        ArrayList<String> cList = new ArrayList<>(Arrays.asList(s.split("\n")));
        this.model.setReplacementList(cList);
        
    } // end method
    
} // end class