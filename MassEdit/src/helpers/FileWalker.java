/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;


import Controller.MainController;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/**
 * @author Andrew Brown
 *
 */
public class FileWalker implements FileVisitor<Path> {
	
	protected ArrayList<String> acceptDirectory;
	protected String targetFileName;
	protected ArrayList<Path> results = new ArrayList<Path>();
        protected MainController controller;
	
	public FileWalker() { }
	
	public FileWalker(ArrayList<String> a, String tf) {
		this.acceptDirectory = a;
		this.targetFileName = tf;
	}
	
	public void filterResults() {
		
		Iterator<Path> it = results.iterator();
		
		while ( it.hasNext() ) {
			
			String abPath = it.next().toAbsolutePath().toString();
			
			for ( String s : this.acceptDirectory ) {
				
				if ( abPath.toLowerCase().contains(s.toLowerCase()) ) {
					continue;
				}
				else {
					it.remove();
					break;
				}
				
			} // end loop
			
		} // end while
		
	} // end method
	
	public void filterResultsAny() {
		
		Iterator<Path> it = results.iterator();
		int len = acceptDirectory.size();
		boolean[] contains = new boolean[len];
		boolean foundDirectory;
		String abPath;
		
		while ( it.hasNext() ) {
			
			Arrays.fill(contains, false);
			abPath = it.next().toAbsolutePath().toString();
			
			for ( int i = 0; i < len; i++ ) {
				
				if ( abPath.toLowerCase().contains(acceptDirectory.get(i).toLowerCase()) ) {
					contains[i] = true;
				}
				
			} // end loop
			
			foundDirectory = containsTrue(contains);

			if ( !foundDirectory ) {
				it.remove();
			}
			
		} // end while

	} // end method
	
	private static boolean containsTrue(boolean[] arr) {
		
		for ( boolean b : arr ) {
			if (b) {
				return true;
			}
		}
		
		return false;
		
	} // end method
		
	public ArrayList<Path> getResults() {
		return this.results;
	}
	
	public void setIgnorList(ArrayList<String> ig) {
		this.acceptDirectory = ig;
	}
	
	public ArrayList<String> getIgnoreList() {
		return this.acceptDirectory;
	}
	
	public void setTargetFileName(String name) {
		this.targetFileName = name;
	}
	
	public String getTargetFileName() {
		return this.targetFileName;
	}

	@Override
	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {		
		return FileVisitResult.CONTINUE;		
	} // end method

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		
		if ( file.toAbsolutePath().toString().contains(this.targetFileName) ) {
			this.results.add(file);
		}
		
		return FileVisitResult.CONTINUE;
		
	} // end method

	@Override
	public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
		exc.printStackTrace();
		return FileVisitResult.valueOf(file.toAbsolutePath().toString() + " failed!");
	}

	@Override
	public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
		return FileVisitResult.CONTINUE;
	}
	
} // end class