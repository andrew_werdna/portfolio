/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import Controller.MainController;
import java.util.ArrayList;
import java.util.List;
import java.util.Observer;
import javax.swing.JFileChooser;

/**
 *
 * @author RLBrown
 */
public class EditorModel {
    
    private JFileChooser chooser;
    private String rootPathForSearch;
    private ArrayList<String> filterDirs;
    private String initialContext;
    private ArrayList<String> initialContextList;
    private String replacement;
    private ArrayList<String> replacementList;
    private MainController controller;	
    
    public EditorModel() {
        this.chooser = new JFileChooser();
    }
    
    public MainController getController() {
        return this.controller;
    }

    public JFileChooser getChooser() {
        return chooser;
    }

    public void setChooser(JFileChooser chooser) {
        this.chooser = chooser;
    }

    public String getRootPathForSearch() {
        return rootPathForSearch;
    }

    public void setRootPathForSearch(String rootPathForSearch) {
        this.rootPathForSearch = rootPathForSearch;
    }

    public ArrayList<String> getFilterDirs() {
        return filterDirs;
    }

    public void setFilterDirs(ArrayList<String> filterDirs) {
        this.filterDirs = filterDirs;
    }

    public String getInitialContext() {
        return initialContext;
    }

    public void setInitialContext(String initialContext) {
        this.initialContext = initialContext;
    }

    public ArrayList<String> getInitialContextList() {
        return initialContextList;
    }

    public void setInitialContextList(ArrayList<String> initialContextList) {
        this.initialContextList = initialContextList;
    }

    public String getReplacement() {
        return replacement;
    }

    public void setReplacement(String replacement) {
        this.replacement = replacement;
    }

    public ArrayList<String> getReplacementList() {
        return replacementList;
    }

    public void setReplacementList(ArrayList<String> replacementList) {
        this.replacementList = replacementList;
    }   
    
} // end class