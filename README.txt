add_snippets.py (PYTHON) is a simple command line tool to loop through funnel directories and check/add analytics snippets
in proper places on each offer page (each page that the user sees)

funnel_build_automation.jar (JAVA) is an executable that yields a GUI for hosting of design pages, adding analytics snippets
to each offer page in a given directory, as well as building .ini files for a configuration set up for a PHP class

csv_automation (PHP) is the utilization of several hand-written classes to automate
the simple administrative task of clearing dozens of CSV files of recent orders

TriangleOrderClear (JAVA) is a Selenium-based program for clearing up to 1,000 datasets of test data from the Triangle CRM