package pages;

import java.util.HashMap;

/**
 * @author hawkgroup
 *
 */
public class Page {
	
	protected HashMap<String, String> selectors;
	
	public Page() { }
	
	public Page(HashMap<String, String> ch) {
		this.selectors = ch;
	}
	
	public HashMap<String, String> getChildren() {
		return selectors;
	}
	
	public void setChildren(HashMap<String, String> ch) {
		this.selectors = ch;
	}
	
	public String getSelectorFor(String key) {
		return selectors.get(key);
	}
	
	public void setSelectorFor(String key, String value) {
		selectors.put(key, value);
	}
	
} // end class