package pages;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageHandler {

	protected WebDriver driver;
	protected WebDriverWait wait;
	protected Page currentPage;
	protected HashMap<String, Page> pageList = new HashMap<String, Page>();
	protected String parentWindow;
	
	public PageHandler() { }
	
	public PageHandler(Page p) {
		this.currentPage = p;
	}
	
	public PageHandler(HashMap<String, Page> pList) {
		this.pageList = pList;
	}
	
	public HashMap<String, Page> getPageList() {
		return this.pageList;
	}
	
	public void addPageToList(String k, Page p) {
		this.pageList.put(k, p);
	}
	
	public Page getPageFromList(String key) {
		return this.pageList.get(key);
	}
	
	public PageHandler(Page p, WebDriver d, WebDriverWait wd) {
		this.currentPage = p;
		this.wait = wd;
		this.driver = d;
	}
	
	public void setCurrentPage(Page p) {
		this.currentPage = p;
	}
	
	public Page getPage() {
		return this.currentPage;
	}
	
	public void setDriver(WebDriver d) {
		this.driver = d;
	}
	
	public WebDriver getDriver() {
		return this.driver;
	}
	
	public void setExplicitWait(WebDriverWait wdw) {
		this.wait = wdw;
	}
	
	public WebDriverWait getExplicitWait() {
		return this.wait;
	}
	
	public WebElement getPageElement(String value) throws TimeoutException, NullPointerException {
		
		WebElement w = null;
	
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(value)));
		w = driver.findElement(By.cssSelector(value));
		
		return w;
		
	}
	
	public void clickElement(String value) {
		
		try {
			getPageElement(value).click();
		}
		catch ( WebDriverException | NullPointerException wde ) {
			wde.printStackTrace();
			System.out.println("problem finding element: " + value);
		}
		
	}
	
	public void clickElement(WebElement we) {
		
		try {
			wait.until(ExpectedConditions.visibilityOf(we));
			we.click();
		}
		catch ( WebDriverException wde ) {
			wde.printStackTrace();
			System.out.println("unable to click element!" + we.toString());
		}
		
	} // end method
	
	public void fillField(String input, String value) {
		getPageElement(input).sendKeys(value);
	}
	
	public void selectOption(WebElement we, String option) {
		new Select(we).selectByVisibleText(option);
	}
	
	public void clearInput(WebElement we) {
			
		we.sendKeys(Keys.COMMAND + "a");
		we.sendKeys(Keys.DELETE);
		we.sendKeys(Keys.CONTROL + "a");
		we.sendKeys(Keys.DELETE);
		
	} // end method
	
	public String getText(WebElement we) {
		return we.getText();
	}
	
	public List<WebElement> getPageElements(String value) {
		
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(value)));
		}
		catch ( WebDriverException wde ) {
			wde.printStackTrace();
		}
		
		return driver.findElements(By.cssSelector(value));
		
	}
	
	public void setParentWindow(String s) {
		this.parentWindow = s;
	}
	
	public String getParentWindow() {
		return this.parentWindow;
	}
	
	public boolean elementExists(String value) {
		return driver.findElements(By.cssSelector(value)).size() != 0;
	}
	
	public void switchWindow(String winHandle, Page p) {
		this.currentPage = p;
		driver.switchTo().window(winHandle);
	}
	
	public void closeWindow() {
		driver.close();
	}
	
} // end class
