package selenium_order_clear;

import javax.swing.JOptionPane;

public class InputHandler {
	
	public static String getDate() {
		return JOptionPane.showInputDialog("Please Enter The Search Beginning Date\n"
				+ "in format mm/dd/yyyy\n"
				+ "enter nothing if you want to \nonly search for today's orders");
	}
	
} // end class