/**
 * 
 */
package selenium_order_clear;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * @author Andrew Brown
 *
 */
public class JSONReader {
	
	public static JSONObject GetJson(String filePath) {
		
		FileReader reader = null;
		JSONObject jsonObj = null;
		
		try {
			reader = new FileReader(filePath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		try {
			jsonObj = (JSONObject) new JSONParser().parse(reader);
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
		
		
		return jsonObj;
		
	} // end method
	
	public static String getJsonValue(JSONObject jsonObj, String key) {
		return (String)jsonObj.get(key);
	}
	
	public static String getFileToRead() {
		return System.getProperty("user.dir") + 
				File.separator + "credentials.json";
	}
	
} // end class