package selenium_order_clear;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.json.simple.JSONObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.Page;
import pages.PageHandler;

public class OrderClear {

	public static void main(String[] args) {
		
		String date = null;
		date = InputHandler.getDate();
		int numResultsPages;
		
		WebDriver driver = new FirefoxDriver();
		WebDriverWait explicitWait = new WebDriverWait(driver, 10);
		List<WebElement> purchaseLinks = null;
		Page logIn = new Page();;
		Page dashboard = new Page();;
		Page search = new Page();;
		Page results = new Page();;
		Page eachOrder = new Page();;
		
		PageHandler ph = new PageHandler();
		ph.setDriver(driver);
		ph.setExplicitWait(explicitWait);
		
		// values for navigating through triangle
		HashMap<String, String> logInNeeds = new HashMap<String, String>();
		HashMap<String, String> dashboardNeeds = new HashMap<String, String>();
		HashMap<String, String> searchPageNeeds = new HashMap<String, String>();
		HashMap<String, String> resultsNeeds = new HashMap<String, String>();
		HashMap<String, String> eachOrderNeeds = new HashMap<String, String>();
		
		// these are for logging in
		logInNeeds.put("url", "https://hawkgroup.trianglecrm.com/");
		logInNeeds.put("userName", "input[name='tbxLogin']");
		logInNeeds.put("userPass", "input[name='tbxPassword']");
		logInNeeds.put("logInButton", "input[name='btnLogin']");
		logIn.setChildren(logInNeeds);
		
		// these are for going to advanced search
		dashboardNeeds.put("search", "#searchLink");
		dashboardNeeds.put("advancedSearch", "[href*='AdvancedSearch.aspx']");
		dashboard.setChildren(dashboardNeeds);
		
		// these are for searching by name
		searchPageNeeds.put("beginningDate", "input[name='ctl00$MainContent$DateRangeControl1$from']");
		searchPageNeeds.put("searchDate", date);
		searchPageNeeds.put("searchIn", "#ddlEntity_title");
		searchPageNeeds.put("allOrdersText", "All");
		searchPageNeeds.put("nameSelector", "#ddlEntity_child > ul:nth-child(1) > li:nth-child(4)");
		searchPageNeeds.put("valueField", "#tbxSearchText");
		searchPageNeeds.put("searchName", "Test");
		searchPageNeeds.put("addSearchCriteria", "#addSearchCriteria");
		searchPageNeeds.put("activeSearchSelector", "input.blueBtnSmall.floatRight[value='Search']");
		searchPageNeeds.put("quantity", "#le_tablePager_center select");
		search.setChildren(searchPageNeeds);
		
		// this is for the search result set
		resultsNeeds.put("q500", "select[role='listbox']");
		resultsNeeds.put("searchResults", "a[href*='CustomerProfile.aspx']");
		resultsNeeds.put("numPages", "#sp_1_le_tablePager");
		resultsNeeds.put("nextResultPage", "#next_le_tablePager > .ui-icon.ui-icon-seek-next");
		results.setChildren(resultsNeeds);
		
		// this is for each individual purchase
		eachOrderNeeds.put("membershipLinkSelector", "a.txtBlue:nth-child(3)");
		eachOrderNeeds.put("cancelLinkConfirmation", "#yesButton");
		eachOrderNeeds.put("closeCancellationDialog", "#cboxClose");
		eachOrderNeeds.put("adminButton", "#ui-id-5");
		eachOrderNeeds.put("voidAsTest", "input[name='ctl03']");
		eachOrder.setChildren(eachOrderNeeds);
		
		ph.addPageToList("logInPage", logIn);
		ph.addPageToList("dashboardPage", dashboard);
		ph.addPageToList("searchPage", search);
		ph.addPageToList("resultsPage", results);
		ph.addPageToList("orderPage", eachOrder);
		
		ph.setCurrentPage(logIn);
		openTriangle(ph);
		ph.setCurrentPage(dashboard);
		advancedSearch(ph);
		ph.setCurrentPage(search);
		searchDate(ph);
		searchByName(ph);
		ph.setCurrentPage(results);
		//maxSearchList(ph);		
		
		numResultsPages = getNumPagesOfResults(ph);
		ph.setParentWindow(driver.getWindowHandle());
		
		for ( int i = 0; i < numResultsPages; i++ ) {
			
			purchaseLinks = ph.getPageElements(resultsNeeds.get("searchResults"));
			
			if ( purchaseLinks.size() > 0 ) {
				
				for ( WebElement e : purchaseLinks ) {
					
					if ( e.isDisplayed() ) {
						System.out.println("clicking order link");
						ph.clickElement(e);
						ph.setCurrentPage(eachOrder);
						voidPurchases(ph);
					}
					else {
						continue;
					}
					
				} // end loop
				
			} // end if
			
			ph.clickElement(resultsNeeds.get("nextResultPage"));
			
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		} // end loop
		
		JOptionPane.showMessageDialog(null, "Program Complete");

	} // end MAIN method
	
	public static void wait(WebDriver d, int secs) {
		d.manage().timeouts().implicitlyWait(secs, TimeUnit.SECONDS);
	}
	
	public static void openTriangle(PageHandler p) {
		
		HashMap<String, String> lg = p.getPage().getChildren();
		
		String fileToRead = JSONReader.getFileToRead();
		//System.out.println(fileToRead);
		JSONObject jsonObj = null;
		String userName = null;
		String password = null;
		WebElement cbc = null;
		
		// Launch website
		p.getDriver().navigate().to(lg.get("url"));
		
		// Maximize the browser
		p.getDriver().manage().window().maximize();
		
		try {
			jsonObj = JSONReader.GetJson(fileToRead);
			userName = JSONReader.getJsonValue(jsonObj, "username");
			password = JSONReader.getJsonValue(jsonObj, "password");
		} catch ( Exception e ) {
			
		}
		
		p.fillField(lg.get("userName"), userName);
		p.fillField(lg.get("userPass"), password);
		
		p.clickElement(lg.get("logInButton"));
		
		// look for triangle pop up and close it if it's there
		
		try {
			cbc = p.getPageElement("#cboxClose");
		}
		catch ( WebDriverException | NullPointerException wde ) {
			wde.printStackTrace();
			System.out.println("could not find element: #cboxClose");
		}
		
		if ( cbc != null ) { 
			p.clickElement("#cboxClose");
		}
		
	} // end method
	
	public static void advancedSearch(PageHandler p) {		
		
		HashMap<String, String> lg = p.getPage().getChildren();
		p.clickElement(lg.get("search"));		
		p.clickElement(lg.get("advancedSearch"));
		
	} // end method
	
	public static void searchDate(PageHandler p) {
		
		HashMap<String, String> lg = p.getPage().getChildren();
		String dateToStartAt = lg.get("searchDate");
		String beginningDateSelector = lg.get("beginningDate");
		
		if ( dateToStartAt == null ) {
			return;
		}
		
		WebElement dateBeginning = p.getPageElement(beginningDateSelector);
		p.clearInput(dateBeginning);
		p.fillField(beginningDateSelector, dateToStartAt);
		
	} // end method
	
	public static void maxSearchList(PageHandler p) {
		
		HashMap<String, String> lg = p.getPage().getChildren();
		WebElement listLength = p.getPageElement(lg.get("q500"));
		p.selectOption(listLength, "500");
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	} // end method
	
	public static int getNumPagesOfResults(PageHandler p) {
		
		HashMap<String, String> lg = p.getPage().getChildren();
		WebElement pages = p.getPageElement(lg.get("numPages"));		
		return Integer.parseInt(p.getText(pages));
		
	} // end method
	
	public static void searchByName(PageHandler p) {
		
		HashMap<String, String> lg = p.getPage().getChildren();
		p.clickElement(lg.get("searchIn"));
		
		wait(p.getDriver(), 2);
		
		p.clickElement(lg.get("nameSelector"));
		
		wait(p.getDriver(), 2);
		
		p.fillField(lg.get("valueField"), "Test");
		
		wait(p.getDriver(), 2);
		
		p.clickElement(lg.get("addSearchCriteria"));
		p.clickElement(lg.get("activeSearchSelector"));
		
	} // end method
	
	public static void voidPurchases(PageHandler p) {
		
		HashMap<String, String> values = p.getPage().getChildren();
		WebElement cancelMembership = null;
		String parentWindow = p.getParentWindow();
			
		try {
			
			try {
				Thread.sleep(300);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			System.out.println("switching to new order window");
			for ( String handle : p.getDriver().getWindowHandles() ) {
				p.switchWindow(handle, p.getPageFromList("orderPage"));
			}			
			
			try {
				
				if ( p.elementExists(values.get("membershipLinkSelector")) ) {
					
					cancelMembership = p.getPageElement(values.get("membershipLinkSelector"));
					
				}
				
			} catch ( WebDriverException | NullPointerException wde ) {
				wde.printStackTrace();
				//p.switchWindow(parentWindow, p.getPageFromList("resultsPage"));
			}
			
			if ( cancelMembership != null ) {

				cancelMembership.click();
				
				try {
					
						wait(p.getDriver(), 1);
						p.clickElement(values.get("cancelLinkConfirmation"));
	
				}
				catch ( WebDriverException | NullPointerException wde ) {
					wde.printStackTrace();
					wait(p.getDriver(), 1);
					//p.switchWindow(parentWindow, p.getPageFromList("resultsPage"));
				}
				finally {
					p.clickElement(values.get("closeCancellationDialog"));
					wait(p.getDriver(), 2);
				}

			} // end if
			
			try {
				
				System.out.println("clicking admin button");
				p.clickElement(values.get("adminButton"));
				System.out.println("success!\nvoiding as test now");
				p.clickElement(values.get("voidAsTest"));				
				System.out.println("success!");
				
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				p.closeWindow();
				//System.out.println("closing window");
				p.switchWindow(parentWindow, p.getPageFromList("resultsPage"));
				//System.out.println("switching window");
				wait(p.getDriver(), 2);

			} catch ( WebDriverException | NullPointerException wde ) {
				wde.printStackTrace();
				p.switchWindow(parentWindow, p.getPageFromList("resultsPage"));
			} 
		
		}
		catch ( WebDriverException ex ) {
			ex.printStackTrace();
		}
		
	} // end method

} // end class
