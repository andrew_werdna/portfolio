import os, sys, re
from pathlib import *

path = sys.argv[1]
ignored = ('xlib', 'TriangleCRM', 'privacy', 'css', 'img', 'js_tmr', 'images', 'res', 'secure_files', 'offers', 'assets')

vwoSnippet = """\n\n<!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=14797,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE 
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->\n\n"""

optimizelySnippet = """\n\n<script src="//cdn.optimizely.com/js/1501167462.js"></script>\n\n"""

gtmSnippetSL = """\n\n<!-- Google Tag Manager -->
<noscript>
<iframe src='//www.googletagmanager.com/ns.html?id=GTM-KQC395'
height='0' width='0' style='display:none;visibility:hidden'>
</iframe>
</noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KQC395');</script>
<!-- End Google Tag Manager -->\n\n"""

gtmSnippetAR = """\n\n<!-- Google Tag Manager -->
<!-- For absoluterights.com ONLY -->
<!-- This is for GTM code and goes IMMEDIATELY aftwer the <body> tag ************* -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MQ97LV"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MQ97LV');</script>
<!-- End Google Tag Manager -->\n\n"""

gtmSnippetPS = """\n\n<!-- This is for GTM code and goes IMMEDIATELY aftwer the <body> tag ************* -->
<!-- For pioneersettler.com ONLY -->
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5FQB9M"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5FQB9M');</script>
<!-- End Google Tag Manager -->\n\n"""

gtmSnippetAGA = """\n\n<!-- This is the NEW GTM code and would go IMMEDIATELY after the <body> tag ********* -->
<!-- FOR gunassociation.org ONLY for now -->
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-573DDJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-573DDJ');</script>
<!-- End Google Tag Manager -->\n\n"""

gtmSnippetABA = """\n\n<!-- Google Tag Manager -->
<!-- This is the NEW GTM code and would go IMMEDIATELY after the <body> tag ********* -->
<!-- FOR beautyassociation.org ONLY for now -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-53J5XL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-53J5XL');</script>
<!-- End Google Tag Manager -->\n\n"""

gtmSnippetNCS = """\n\n<!-- Google Tag Manager -->
<!-- FOR nationalcraftssociety.org ONLY for now -->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-KMMV6L"
height="0" width="0" style="display:none;visibility:hidden">
</iframe>
</noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KMMV6L');</script>
<!-- End Google Tag Manager -->\n\n"""

def getPossibleSnippetsDirectories(path):

	directories = []

	for root, dirs, files in os.walk(path):

		ending = root.split("\\")
		ending.reverse()
	
		if ending[0] in ignored:
			continue

		directories.append(root)

	return directories
	#end function *************************

def hitFile(filePath, action = "find analytics", mode = "r+"):

	if not os.path.isfile(filePath):
		return

	with open(filePath, mode) as f:

		fileName = f.name

		if ( 'find analytics' in action ):
			f.close()
			findAnalytics(f)
		
	print("\n", fileName, " completed ***")
	#end function *************************

def addVWO(fOb, vSnip = vwoSnippet):

	print("Adding VWO to the <head> section...\n")

	with open(fOb.name, 'r+', encoding = 'utf8') as f:

		contents = f.readlines()
		
		for each in contents:
			
			if ( "</head>" in each ):
				hSearch = each		
		
		hd = contents.index(hSearch)
		insertionIndex = hd

		contents.insert(insertionIndex, vSnip)
		contents.insert(insertionIndex, "\n")
		newContents = ''.join(contents)

		f.seek(0, 0)
		f.write(newContents)

	#end function *************************

def addOptimizely(fOb, oSnip = optimizelySnippet):

	print("Adding Optimizely to the <head> section...\n")

	with open(fOb.name, 'r+', encoding = 'utf8') as f:

		contents = f.readlines()
		
		for each in contents:
			
			if ( "</head>" in each ):
				hSearch = each		
		
		hd = contents.index(hSearch)
		insertionIndex = hd

		contents.insert(insertionIndex, oSnip)
		newContents = ''.join(contents)

		f.seek(0, 0)
		f.write(newContents)

	#end function *************************

def addGTM(fOb, gSnip = gtmSnippetSL):

	print("Adding GTM to the <body> section...\n")

	if "gunassociation" in fOb.name:
		gSnip = gtmSnippetAGA
	elif "beautyassociation" in fOb.name:
		gSnip = gtmSnippetABA
	elif "absoluterights" in fOb.name:
		gSnip = gtmSnippetAR
	elif "pioneersettler" in fOb.name:
		gSnip = gtmSnippetPS
	elif "survivallife" in fOb.name:
		gSnip = gtmSnippetSL
	elif "nationalcraftssociety" in fOb.name:
		gSnip = gtmSnippetNCS
	else:
		gSnip = gtmSnippetSL

	with open(fOb.name, 'r+', encoding = 'utf8') as f:

		contents = f.readlines()
		
		for each in contents:
			
			if ( "<body" in each ):
				hSearch = each		
		
		hd = contents.index(hSearch)
		insertionIndex = hd+1

		contents.insert(insertionIndex, gSnip)

		newContents = ''.join(contents)

		f.seek(0, 0)
		f.write(newContents)

	#end function *************************

def findAnalytics(fOb, vwo = "var _vwo_code=(function(){", optimizely = '//cdn.optimizely.com', gtm = "//www.googletagmanager.com/"):

	vwoFound = False
	optimizelyFound = False
	gtmFound = False

	with open(fOb.name, fOb.mode, encoding = 'utf8') as f:

		print("\nNow parsing ", f.name, "\n")

		for line in iter(f.readline, ''):
			if ( vwo in line ):
				vwoFound = True
			elif ( optimizely in line ):
				optimizelyFound = True
			elif ( gtm in line ):
				gtmFound = True
				break

	if ( vwoFound == True ):
		print("VWO is on the page")
	else:
		print("VWO not found.")
		addVWO(f)
	if ( optimizelyFound == True ):
		print("Optimizely is on the page")
	else:
		print("Optimizely Not found. Adding to <head>...")
		addOptimizely(f)
	if ( gtmFound == True ):
		print("GTM is on the page")
	else:
		print("GTM not found. Adding to <body>...")
		addGTM(f)
	
	#end function *************************

def getRelevantFiles(dir):

	fList = []
	p = Path(dir)

	for root, dirs, files in os.walk(dir):

		ending = root.split("\\")
		ending.reverse()
	
		if ending[0] in ignored:
			continue
		
		for f in files:

			if ("php" in f) or ("html" in f):
				
				fullPath = str(p.resolve()) + "\\" + str(f)
				fList.append(fullPath)
	
	return list(set(fList))
	#end function *************************

def mainFunc(path):

	if os.access(path, os.R_OK):

		filez = []
		ds = getPossibleSnippetsDirectories(path)
		
		for d in ds:
			
			f = getRelevantFiles(d)
			
			if ( len(f) > 0 ):
				
				filez.append(f)

		filez = [item for sublist in filez for item in sublist]
		
		for n in filez:
			hitFile(n)
		#end function *************************

mainFunc(path)