<?php
/*
for testing:

https://andrewbrown.local/products/t/best-patriot-by-choice-political-shirt/results.csv
https://andrewbrown.local/gear-skills/t/best-survival-life-shirt/results.csv
https://andrewbrown.local/products/t/lipstick-shirt-black/results.csv
https://andrewbrown.local/products/t/dads-with-pretty-daughters-t-shirt/results.csv
https://andrewbrown.local/products/t/plant-gardens-shirt/results.csv
https://andrewbrown.local/shirts/t/I-make-stuff/results.csv

*/
include_once("../lib/bootstrap.php");

if ( !empty($_POST) ) {

	$updaters = SubmitHandler::csvSetUp($_POST);
	//cleanOutput("updaters", $updaters);
	$removalDate = $updaters['dateToRemoveThrough'];
	cleanOutput("removalDate", $removalDate);

	foreach ($updaters['csvFiles'] as $key => $url) {
		
		if ( strlen($url) < 5 ) {
			continue;
		}

		$pc = new PathConverter($url);
		$pc->setPath($url);
		$path = $pc->getPath();
		//cleanOutput("path", $path);
		//cleanOutput("decoct(fileperms(path) & 0777)", decoct(fileperms($path) & 0777)); // trying to determine if there is permission situation

		$dc = new DateConverter($removalDate);
		$clearDate = $dc->getDateToClear();
		//cleanOutput("clearDate", $clearDate);
		$currYear = $dc->getCurrentYear();
		//cleanOutput("currYear", $currYear);

		if ( !$path ) {
			continue;
		}

		$contents = csvFile::getContents($path, $url);
		//cleanOutput("contents", $contents);

		$newContents = csvFile::clearCSV($contents, $clearDate, $currYear);
		//cleanOutput("newContents", $newContents);

		$replacedContents = csvFile::insertIntoCSV($newContents, $clearDate);
		//cleanOutput("replacedContents", $replacedContents);

		csvFile::writeToCSV($replacedContents, $path, $url);

	} // end foreach
	
} // end if POST is set

?>