<?php
	//$submission = "Test/process.php"; //for the old version
	$submission = "app/process.php";

	// echo dirname(__FILE__);
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]>
<html class="ie ie6 lf-3a" lang="en"> <![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7 lf-3a" lang="en"> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8 lf-3a" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]>
<!-->
<html class="lf-3a" lang="en"> <!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<link rel="profile" href="https://gmpg.org/xfn/11"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
 
<title>Survival Life CSV</title>
<meta property="og:type" content="article"/>
<!--[if (gte IE 6)&(lte IE 8)]>
        <script type="text/javascript" src="https://www.hawkgroupdev.com/racheal/wp-content/themes/optimizePressTheme/lib/js/selectivizr-1.0.2-min.js?ver=1.0.2">
</script>
        <![endif]-->
<!--[if IE]>
<script src="https://html5shiv.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->

<link href="https://fonts.googleapis.com/css?family=Oswald:300,r|Lato:300,r,b,i,bi|Titillium Web:300,r,b,i,bi" rel="stylesheet" type="text/css"/>
 
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<style>
	input, textarea {
		padding: 10px 0px;
		margin: 10px 0px;
	}
</style>

</head>
<body style="background-color:white; padding: 0px; margin: 0px;">
 

<div class="container" style="background-color:gray; padding-bottom:15px;height:700px;">

<h2 style="color: blue; margin:0px; padding: 10px;text-align: center;">CSV Handler</h2>


<form action="<?php echo $submission; ?>" method="post" style="padding: 50px;">
	<textarea placeholder="CSV URLs go here" rows="20" cols="70" name="toBeUpdated" value=""></textarea>
	<br />
	<input placeholder="select date" id="datepicker" type="text" style="padding:10px;" name="dateToRemoveThrough" value="" /><br />
	<input style="float:left;" type="submit" name="submit" value="submit"/>
</form>



</div>



  
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script>
	  $(document).ready(function() {

	    $( "#datepicker" ).datepicker();

	    var form = $("form");

	    form.on("submit", function() {

	    	var inputs = $(":input");

	    	inputs.each(function() {
	    		var val = $(this).val();
	    		var name = $(this).attr("name");

	    		if ( val.length < 1 ) {
	    			return false;
	    		}

	    	});

	    });

	  });
  </script>

</body>
</html>