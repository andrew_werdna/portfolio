<?php

define("_docroot_", $_SERVER['DOCUMENT_ROOT']);
define("_app_dir_", "/csv_automation");
define("_host_", $_SERVER['HTTP_HOST']);

function cleanOutput($varName, $var) {

    echo "<h3>$varName</h3>";
    echo "<pre><tt>";
    var_dump($var);
    echo "</tt></pre><hr><br />";

} // end function

	$files = "classes/files/csvFile.class.php";
	$paths = "classes/files/paths/PathConverter.class.php";
	$dates = "classes/files/dates/DateConverter.class.php";
	$submissions = "classes/submissions/SubmitHandler.class.php";

	include_once($paths);
	include_once($files);
	include_once($dates);
	include_once($submissions);

?>