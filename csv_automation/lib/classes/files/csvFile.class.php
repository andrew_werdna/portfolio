<?php

/**
*   @author Andrew Brown
*
*   Handles CSV I/O including:
*       inserting a notice of removal
*       removing old dates
*       re-writing to a CSV with old content removed
*/
class csvFile {


    protected $pathConverter;
    protected $dateConverter;


    /**
    *   @param $dateConverter : DateConverter
    *   @param $pathConverter : PathConverter
    */
    public function __construct(DateConverter $dateConverter, PathConverter $pathConverter) {
        $this->dateConverter = $dateConverter;
        $this->pathConverter = $pathConverter;
    }


    /**
    *   @return $dateConverter : DateConverter
    */
    public function getDateConverter() {
        return $this->dateConverter;
    }


    /**
    *   @return $pathConverter : PathConverter
    */
    public function getPathConverter() {
        return $this->pathConverter;
    }


	/** 
    *   @param $dataArray : Array ( Array of arrays containing the indexs and columns of CSV AFTER removal of necessary rows )
    *   @param $replaceDate : String
    *   @return $dataArray : Array ( Array of arrays containing the rows and columns of CSV AFTER INSERTION of removal notice ) 
    */
    public static function insertIntoCSV($dataArray, $replaceDate) {

        $index = null;
        $replacementStr = "Removed orders through " . $replaceDate . " by AUTOMATION SCRIPT";
        $replacement[] = explode("!", $replacementStr);

        //cleanOutput("dataArray", $dataArray);
        /*
        for ( $i = 0; $i < count($dataArray); $i++ ) {

            if ( stripos($dataArray[$i][0], "removed") !== false ) { // we found the year
                $index = $i + 1;
            }

        } // end loop
        */
        $index = 1;
        array_splice($dataArray, $index, 0, $replacement );
        //cleanOutput("dataArray", $dataArray);
        
        return $dataArray;

    } // end method
    

    /**
    *   @param $datum : Array ( contents to be written )
    *   @param $path : String ( the path to the file )
    *   @param $url : String ( the original given URL )
    *   @return $result : Boolean
    */
    public static function writeToCSV($datum, $path, $url) {

        $result = false;

        if ( file_exists($path) && is_file($path) ) {

            $file = fopen($path, 'w');

            foreach ( $datum as $rows => $cols ) {
	            //cleanOutput("cols", $cols);
	            fputcsv($file, $cols);
	        }

	        fclose($file);
	        $result = true;

        }
        else {
            echo "<a target='_blank' href='$url'>$url</a> <b><u>is not a valid file!</u></b><br />";
            $result = false;
        }

        echo "Successfully re-wrote to <a href='$url' target='_blank'>$url</a><br />";
        return $result;

    } // end method


    /**
    *   @return formatted date String
    */
    private static function callConversion($str) {
    	return DateConverter::stringToDate($str);
    }


	/**
    *   @param $contents : Array ( array of arrays containing rows and columns of CSV file )
    *   @param $replacementDate : String ( Date to remove through in MM/DD/YYYY format )
    *   @param $currentYear : String
    *   @return $newestContents : Array ( the contents with old rows removed )
    */
    public static function clearCSV($contents, $replacementDate, $currentYear) {
        
        $ly = strval($currentYear-1);

        foreach ( $contents as $index => $row ) {

            if ( strpos($row[0], "removed") !== false ) {
                unset($contents[$index]);
            }            
            elseif ( stripos($row[0], $currentYear) !== false ) {
                
                if ( self::callConversion($row[0]) <= $replacementDate ) {
                    //cleanOutput("column", $column);
                    unset($contents[$index]);
                }

            }
            elseif ( stripos($row[0], $ly) !== false ) {
                unset($contents[$index]);
            }

        }

        $contents = array_values($contents);
        
        //cleanOutput("newestContents", $newestContents);
        
        return $contents;                 

    } // end method


	/**
    *   @param $filename : String ( the string representing the path to the file )
    *   @param $url : String ( the original URL of the file )
    *   @return $contents : Array ( an array of arrays containing each row and the columns of CSV file )
    */
    public static function getContents($filename, $url) {

        $f = preg_replace("/\//", DIRECTORY_SEPARATOR, $filename);

        if ( file_exists($filename) && is_file($filename) ) {

        	$contents = array();
            $file = fopen($f, 'r');

            while ( ($data = fgetcsv($file)) !== false ) {
	            $contents[] = $data;
	        }

	        fclose($file);

        }
        else {
            echo "<a target='_blank' href='$url'>$url</a> <b><u>is not a valid file!</u></b><br />";
            $contents = false;
        }
        
        return $contents;
        
    } // end method


} // end class