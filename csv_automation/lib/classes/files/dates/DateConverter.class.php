<?php

/**
*	@author Andrew Brown
*
*	Stores date information and can
*	convert Strings of valid dates to 
*	a more readable format
*/
class DateConverter {

	protected $currentYear;
	protected $dateToClear;


	/**
	*	@param $date : String ( the date to be cleared THROUGH )
	*/
	public function __construct($date) {

		$this->currentYear = date('Y');
		$this->dateToClear = $date;

	} // end constructor


	/**
	*	@return $currentYear
	*/
	public function getCurrentYear() {
		return $this->currentYear;
	} // end method


	/**
	*	@return $dateToClear
	*/
	public function getDateToClear() {
		return $this->dateToClear;
	} // end method


    /**
    *	@param $dateString : String representing date
    *	@param $format : String representing desired output format
    *	@return $result : FALSE || String depending on success of conversion
    */
    public static function stringToDate($dateString, $format = 'm/d/Y') {

    	if ( ($timeStamp = strtotime($dateString)) === false) {
			$result = false;
		}
		else {
			$result = date($format, $timeStamp);
		}

		return $result;

    } // end method


} // end class