<?php

/**
*	@author Andrew Brown
*	
*	Stores URL's and Paths safely and can
*	convert a URL to a Server Path
*/
class PathConverter {

	protected $url;
	private $path;


	/**
	*	@param $url : String
	*/
	public function __construct($url) {
		$this->url = $url;
	}


	/**
	*	@param $url : String
	*/
	public function setPath($url) {
		$this->path = self::convertURLtoFile($url);
	}


	/**
	*	@return $url property
	*/
	public function getOriginalUrl() {
		return $this->url;
	}


	/**
	*	@return $path property
	*/
	public function getPath() {
		return $this->path;
	}


	/**
    *   
    *   @param $url : String
    *   @return path : String ( the path to the file on the server ) || FALSE
    *   
    */
	private static function convertURLtoFile($url) {
		//cleanOutput("url submitted", $url);
    	$patterns = "((https?:\/\/)(.{5,30})(\.org|\.com|\.local))";
        $partial = preg_replace($patterns, '', $url);
        //cleanOutput("partial after preg_replace", $partial);
        $realFile = trim(_docroot_ . $partial);
		$realFile = preg_replace("/\//", DIRECTORY_SEPARATOR, $realFile);
		//cleanOutput("realFile", $realFile);
		if ( stripos($url, _host_) === false ) {

			$realFile = false;
			echo "<a target='_blank' href='$url'>$url</a> <b><u>is not accessible from " . _host_ . "!</u></b><br />";

		}
        elseif ( !(is_file($realFile) && file_exists($realFile)) ) {

            $realFile = false;
			echo "<a target='_blank' href='$url'>$url</a> <b><u>is not a valid file!</u></b><br />";
            
        } // end if

        return $realFile;
        
    } // end method


} // end class