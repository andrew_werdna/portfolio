<?php

/**
*   @author Andrew Brown
*   
*   Simple utility for form submission that cleans
*   and organizes the data
*/
class SubmitHandler {

    /**
    *   
    *   @param $arry : array ( usually $_POST )
    *   @return $updaters : array ( sanitized and organized )
    *   
    */
    public static function csvSetUp($arry) {

        $updaters = array();

        foreach ( $arry as $k => $v ) {
            $current = preg_replace("/\s+/", "", $v); 
            $fields[$k] = filter_var($current, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        }

        //cleanOutput("fields", $fields);
        
        if ( (stripos($fields['toBeUpdated'], "\n") !== false) && (stripos($fields['toBeUpdated'], ",") !== false) ) { // found both \n and ,
            $fields['toBeUpdated'] = str_ireplace(",", "", $fields['toBeUpdated']);
            $updaters['csvFiles'] = explode("\n", $fields['toBeUpdated']);
        }
        elseif ( stripos($fields['toBeUpdated'], "\n") !== false ) { // found newline character
            $updaters['csvFiles'] = explode("\n", $fields['toBeUpdated']);
        }
        elseif ( stripos($fields['toBeUpdated'], ",") !== false ) { // found comma
            $updaters['csvFiles'] = explode(",", $fields['toBeUpdated']);
        }
        else {
            $updaters['csvFiles'] = explode("!", $fields['toBeUpdated']); // create single-index array
        }
        
        $updaters['dateToRemoveThrough'] = $fields['dateToRemoveThrough'];

        foreach ( $updaters['csvFiles'] as $k => $v ) {
            $updaters['csvFiles'][$k] = trim($v);
        }

        return $updaters;

    } // end method


} // end class