<?php

/**
*   @author Andrew Brown
*   
*   Simple utility for form submission that cleans
*   and organizes the data
*/
class SubmitHandler {

    /**
    *   
    *   @param $arry : array ( usually $_POST )
    *   @return $updaters : array ( sanitized and organized )
    *   
    */
    public static function csvSetUp($arry) {

        $updaters = array();

        foreach ( $arry as $k => $v ) {    
            //$current = preg_replace("/\s+/", "", $v);        
            $fields[$k] = filter_var($v, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        }
        //cleanOutput("fields['toBeUpdated'] after filter_var", $fields['toBeUpdated']);
        //$fields['toBeUpdated'] = preg_replace("/\s+/", "", $fields['toBeUpdated']);
        //cleanOutput("fields['toBeUpdated'] after whitespace removal", $fields['toBeUpdated']);

        $len = strlen($fields['toBeUpdated']);

        if ( (stripos($fields['toBeUpdated'], "\n") !== false) && (stripos($fields['toBeUpdated'], ",") !== false) ) { // found both \n and ,
            $fields['toBeUpdated'] = str_ireplace("\n", "", $fields['toBeUpdated']);
            $updaters['csvFiles'] = explode(",", $fields['toBeUpdated']);
        }
        elseif ( stripos($fields['toBeUpdated'], "\n") !== false ) { // found newline character
            $updaters['csvFiles'] = explode("\n", $fields['toBeUpdated']);
        }
        elseif ( stripos($fields['toBeUpdated'], ",") !== false ) { // found comma
            $updaters['csvFiles'] = explode(",", $fields['toBeUpdated']);;
        }
        else {
            $updaters['csvFiles'] = explode("!", $fields['toBeUpdated']); // create single-index array
        }

        //cleanOutput("updaters['csvFiles'] before trim", $updaters['csvFiles']);
        $updaters['dateToRemoveThrough'] = $fields['dateToRemoveThrough'];

        foreach ( $updaters['csvFiles'] as $k => $v ) {
            $updaters['csvFiles'][$k] = trim($v);
        }
        //cleanOutput("updaters['csvFiles'] after explosion and more filtering", $updaters['csvFiles']);
        return $updaters;

    } // end method


} // end class