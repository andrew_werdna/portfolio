import os, sys
from pathlib import *

directories = []
file_list = []
files_containing = []
images = ['.gif', '.jpg', '.jpeg', '.png', '.psd', '.svg']
# search_string = "$si->process"
# path_used = "C:\\xampp\\htdocs\\survivallife.local\\gear-skills\\t\everstryke-ll"
path_used = sys.argv[1]
search_string = sys.argv[2]

for root, dirs, files in os.walk(path_used):
	if 'images' in root:
		continue
	directories.append(root)

for each in directories:
	for root, dirs, files in os.walk(each):

		ending = root.split("\\")
		ending.reverse()

		if ending[0] in images:
			continue

		p = Path(root)
		for f in files:
			fullPath = str(p.resolve()) + "\\" + str(f)
			if os.path.isfile(fullPath):
				if 'images' in fullPath:
					continue
				file_list.append(fullPath)

file_list = list(set(file_list))

for e in file_list:
	with open(e, 'r+', encoding = 'utf-8') as f:
		for line in iter(f.readline, ''):
			if search_string in line:
				#print(e)
				files_containing.append(e)
				break

files_containing = list(set(files_containing))
print("\nTHE FILES THAT CONTAIN YOUR STRING ARE::\n")
for fn in files_containing:
	print(fn + "\n")